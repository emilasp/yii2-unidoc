<?php

use emilasp\unidoc\models\Project;
use emilasp\unidoc\models\Strategy;
use kartik\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\tasks\models\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Html::tag('span', '', ['class' => 'fa fa-list text-primary']) . ' ' . Yii::t('tasks',
        'Projects');
$this->params['breadcrumbs'][] = Yii::t('tasks', 'Projects');
?>
<div class="project-index">

    <p><?= Html::a(Yii::t('tasks', 'Create Project'), ['create'], ['class' => 'btn btn-success']) ?></p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'strategy_id',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return $model->strategy->name;
                },
                'filter'    => Strategy::find()->byCreatedBy()->map()->cache()->orderBy('name')->all(),
            ],

            [
                'attribute' => 'name',
                'format'    => 'raw',
                'value'     => function ($model, $key, $index, $column) {
                    return Html::a($model->name, ['view', 'id' => $model->id],
                        ['class' => '', 'data-pjax' => 0]);
                },
                'class'     => DataColumn::className(),
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '250px',
            ],
            'description:ntext',
            [
                'attribute' => 'progress',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return \yii\jui\ProgressBar::widget(['clientOptions' => ['value' => $model->progress]]);
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'type',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return Project::$types[$model->type];
                },
                'filter'    => Project::$types
            ],
            [
                'attribute' => 'status',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return Project::$statuses[$model->status];
                },
                'filter'    => Project::$statuses
            ],
            [
                'attribute' => 'created_by',
                'value'     => function ($model) {
                    return ArrayHelper::getValue($model->createdBy, 'username', null);
                },
                'class'     => DataColumn::className(),
                'width'     => '150px',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
