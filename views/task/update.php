<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\tasks\models\Task */

$this->title = Html::tag('span', '', ['class' => 'fa fa-pencil text-primary']) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('tasks', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('tasks', 'Update');
?>
<div class="task-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
