<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emilasp\tasks\models\Task */

$this->title = Html::tag('span', '', ['class' => 'fa fa-eye text-info']) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('tasks', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="task-view">

    <p>
        <?= Html::a(Yii::t('site', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('site', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('site', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'strategy_id',
            'project_id',
            'parent_id',
            'name',
            'description:ntext',
            'result:ntext',
            'checklists',
            'progress',
            'priority',
            'type',
            'price',
            'time:datetime',
            'status',
            'started_at',
            'finished_at',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
