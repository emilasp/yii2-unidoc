<?php

namespace emilasp\unidoc\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\media\behaviors\FileBehavior;
use emilasp\media\models\File;
use emilasp\taxonomy\behaviors\TagBehavior;
use emilasp\taxonomy\models\Tag;
use emilasp\taxonomy\models\TagLink;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "unidoc_tasks_strategy".
 *
 * @property integer   $id
 * @property string    $name
 * @property string    $description
 * @property integer   $status
 * @property string    $created_at
 * @property string    $updated_at
 * @property integer   $created_by
 * @property integer   $updated_by
 *
 * @property Project[] $projects
 * @property Task[]    $tasks
 * @property User      $createdBy
 * @property User      $updatedBy
 */
class Strategy extends ActiveRecord
{
    public static $taskType;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'files' => [
                'class'     => FileBehavior::className(),
                'attribute' => 'files',
            ],
            'tags'  => [
                'class' => TagBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unidoc_tasks_strategy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['description'], 'string'],
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
            ['formTags', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('tasks', 'ID'),
            'name'        => Yii::t('tasks', 'Наименование'),
            'description' => Yii::t('tasks', 'Описание'),
            'status'      => Yii::t('tasks', 'Статус'),
            'created_at'  => Yii::t('tasks', 'Создан'),
            'updated_at'  => Yii::t('tasks', 'Изменен'),
            'created_by'  => Yii::t('tasks', 'Автор'),
            'updated_by'  => Yii::t('tasks', 'Изменил'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['strategy_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['strategy_id' => 'id'])->byStatus();
    }

    /**
     * Количество задач
     *
     * @return int|string
     */
    public function getTasksCount()
    {
        return $this->getTasks()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }


    /**
     * @return Query
     */
    public function getTagLink()
    {
        return $this->hasMany(TagLink::className(), ['object_id' => 'id'])
            ->andOnCondition(['object' => self::className()]);
    }

    /**
     * @return Query
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->via('tagLink');
    }

    /**
     * Получаем файлы привязанные к моделе
     *
     * @return Query
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['object_id' => 'id'])->where(['object' => self::className()]);
    }
}
