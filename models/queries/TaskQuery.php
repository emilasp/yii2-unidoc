<?php
namespace emilasp\unidoc\models\queries;

use emilasp\core\components\base\BaseActiveQuery;
use emilasp\unidoc\models\Task;

/**
 * Class TaskQuery
 * @package emilasp\unidoc\models\queries
 */
class TaskQuery extends BaseActiveQuery
{
    /**
     * INIT
     */
    public function init()
    {
        /** @var Task $modelClass */
        $modelClass = $this->modelClass;
        $tableName  = $modelClass::tableName();

        $this->andWhere([$tableName . '.task_type' => $modelClass::$taskType]);

        parent::init();
    }
}
