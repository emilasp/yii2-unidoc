<?php

namespace emilasp\unidoc\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\media\behaviors\FileBehavior;
use emilasp\media\models\File;
use emilasp\taxonomy\behaviors\TagBehavior;
use emilasp\taxonomy\models\Tag;
use emilasp\taxonomy\models\TagLink;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "unidoc_tasks_project".
 *
 * @property integer  $id
 * @property integer  $strategy_id
 * @property string   $name
 * @property string   $description
 * @property integer  $progress
 * @property integer  $type
 * @property integer  $status
 * @property string   $created_at
 * @property string   $updated_at
 * @property integer  $created_by
 * @property integer  $updated_by
 *
 * @property Strategy $strategy
 * @property User     $createdBy
 * @property User     $updatedBy
 * @property Task[]   $tasks
 */
class Project extends ActiveRecord
{
    const TYPE_WITHOUT_TYPE = 0;

    public static $types = [
        self::TYPE_WITHOUT_TYPE => self::TYPE_WITHOUT_TYPE
    ];

    public static $type;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'files' => [
                'class'     => FileBehavior::className(),
                'attribute' => 'files',
            ],
            'tags'  => [
                'class' => TagBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unidoc_tasks_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['strategy_id', 'name', 'status'], 'required'],
            [['strategy_id', 'progress', 'status', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['strategy_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Strategy::className(),
                'targetAttribute' => ['strategy_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
            ['formTags', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('tasks', 'ID'),
            'strategy_id' => Yii::t('tasks', 'Стратегия'),
            'name'        => Yii::t('tasks', 'Наименование'),
            'description' => Yii::t('tasks', 'Описание'),
            'progress'    => Yii::t('tasks', 'Прогресс'),
            'status'      => Yii::t('tasks', 'Статус'),
            'created_at'  => Yii::t('tasks', 'Создан'),
            'updated_at'  => Yii::t('tasks', 'Изменен'),
            'created_by'  => Yii::t('tasks', 'Автор'),
            'updated_by'  => Yii::t('tasks', 'Изменил'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStrategy()
    {
        return $this->hasOne(Strategy::className(), ['id' => 'strategy_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['project_id' => 'id'])->andFilterWhere([
            'type' => static::$type
        ]);
    }

    /**
     * Количество задач
     *
     * @return int|string
     */
    public function getTasksCount()
    {
        return $this->getTasks()->count();
    }

    /**
     * @return Query
     */
    public function getTagLink()
    {
        return $this->hasMany(TagLink::className(), ['object_id' => 'id'])
            ->andOnCondition(['object' => self::className()]);
    }

    /**
     * @return Query
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->via('tagLink');
    }

    /**
     * Получаем файлы привязанные к моделе
     *
     * @return Query
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['object_id' => 'id'])->where(['object' => self::className()]);
    }
}
