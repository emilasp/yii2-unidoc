<?php
namespace emilasp\unidoc\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\tasks\models\Task;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "unidoc_tasks_result".
 *
 * @property integer $id
 * @property integer $task_id
 * @property integer $accumulate
 * @property string  $result
 * @property integer $status
 * @property string  $created_at
 *
 * @property Task    $task
 */
class TaskResult extends ActiveRecord
{
    const STATUS_GOOD = 9;
    const STATUS_NEW  = 10;
    const STATUS_FAIL = 11;

    public static $statuses = [
        self::STATUS_NEW  => 'новый',
        self::STATUS_GOOD => 'отлично',
        self::STATUS_FAIL => 'провал',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [ActiveRecord::EVENT_BEFORE_INSERT => ['created_at']],
                'value'      => new Expression('NOW()'),
            ]
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unidoc_tasks_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['result'], 'string'],
            [['task_id', 'status'], 'required'],
            [['accumulate'], 'integer'],
            [['created_at'], 'safe'],
            [
                ['task_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Task::className(),
                'targetAttribute' => ['task_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'task_id'    => Yii::t('tasks', 'Task'),
            'accumulate' => Yii::t('tasks', 'Accumulate'),
            'result'     => Yii::t('tasks', 'Result'),
            'status'     => Yii::t('site', 'Status'),
            'created_at' => Yii::t('site', 'Created At'),
        ];
    }

    /**
     * Получаем файлы привязанные к моделе
     *
     * @return Query
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id'])->where(['task_type' => Task::TASK_TYPE_TASK]);
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->task->fillMetrics();
        $this->task->setMetrics();
        $this->task->save();
    }
}
