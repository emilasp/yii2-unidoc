<?php

namespace emilasp\unidoc\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\unidoc\models\Task;

/**
 * TaskSearch represents the model behind the search form of `emilasp\unidoc\models\Task`.
 */
class TaskSearch extends Task
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'strategy_id',
                    'project_id',
                    'parent_id',
                    'progress',
                    'priority',
                    'type',
                    'task_type',
                    'make_type',
                    'time',
                    'status',
                    'created_by',
                    'updated_by',
                    'is_argue'
                ],
                'integer'
            ],
            [
                [
                    'name',
                    'description',
                    'result',
                    'checklists',
                    'started_at',
                    'finished_at',
                    'created_at',
                    'updated_at',
                    'metrics'
                ],
                'safe'
            ],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'          => $this->id,
            'strategy_id' => $this->strategy_id,
            'project_id'  => $this->project_id,
            'parent_id'   => $this->parent_id,
            'progress'    => $this->progress,
            'priority'    => $this->priority,
            'type'        => $this->type,
            'task_type'   => $this->task_type,
            'make_type'   => $this->make_type,
            'price'       => $this->price,
            'time'        => $this->time,
            'status'      => $this->status,
            'is_argue'    => $this->is_argue,
            'started_at'  => $this->started_at,
            'finished_at' => $this->finished_at,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
            'created_by'  => $this->created_by,
            'updated_by'  => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'metric', $this->metric])
            ->andFilterWhere(['like', 'checklists', $this->checklists]);

        return $dataProvider;
    }
}
