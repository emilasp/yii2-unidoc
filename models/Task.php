<?php

namespace emilasp\unidoc\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\media\behaviors\FileBehavior;
use emilasp\media\models\File;
use emilasp\taxonomy\behaviors\TagBehavior;
use emilasp\taxonomy\models\Tag;
use emilasp\taxonomy\models\TagLink;
use emilasp\unidoc\models\queries\TaskQuery;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "unidoc_tasks_task".
 *
 * @property integer  $id
 * @property integer  $strategy_id
 * @property integer  $project_id
 * @property integer  $parent_id
 * @property string   $name
 * @property string   $description
 * @property string   $result
 * @property string   $checklists
 * @property integer  $progress
 * @property integer  $priority
 * @property integer  $task_type
 * @property integer  $type
 * @property integer  $make_type
 * @property string   $price
 * @property integer  $time
 * @property string   $metric
 * @property integer  $is_argue
 * @property integer  $status
 * @property string   $started_at
 * @property string   $finished_at
 * @property string   $created_at
 * @property string   $updated_at
 * @property integer  $created_by
 * @property integer  $updated_by
 *
 * @property Strategy $strategy
 * @property Project  $project
 * @property Task     $parent
 * @property Task[]   $tasks
 * @property User     $createdBy
 * @property User     $updatedBy
 */
class Task extends ActiveRecord
{
    const TASK_TYPE_TASK = 1;
    const TASK_TYPE_NOTE = 2;

    public static $taskType;

    public static $taskTypes = [
        self::TASK_TYPE_TASK => 'Задача',
        self::TASK_TYPE_NOTE => 'Заметка',
    ];

    const TYPE_NO_TYPE = 0;
    const TYPE_FROG    = 1;
    const TYPE_BISHOP  = 2;

    public static $types = [
        self::TYPE_NO_TYPE => 'Без типа',
        self::TYPE_FROG    => 'Лягушка',
        self::TYPE_BISHOP  => 'Слон',
    ];

    const MAKE_TYPE_SINGLE   = 1;
    const MAKE_TYPE_REPEAT   = 2;
    const MAKE_TYPE_CONTINUE = 3;

    public static $make_types = [
        self::MAKE_TYPE_SINGLE   => 'Одноразовая',
        self::MAKE_TYPE_REPEAT   => 'Повторяющаяся',
        self::MAKE_TYPE_CONTINUE => 'Продолжительная',
    ];

    const  STATUS_NEW     = 0;
    const  STATUS_JOB     = 1;
    const  STATUS_FINISH  = 2;
    const  STATUS_ANALIZE = 3;
    const  STATUS_CANCEL  = 4;

    public static $statuses = [
        self::STATUS_NEW     => 'Новая',
        self::STATUS_JOB     => 'В работе',
        self::STATUS_FINISH  => 'Завершена',
        self::STATUS_ANALIZE => 'Анализируется',
        self::STATUS_CANCEL  => 'Отменена',
    ];

    const  PRIORITY_LOW      = 1;
    const  PRIORITY_MIDDLE   = 2;
    const  PRIORITY_HIGHT    = 3;
    const  PRIORITY_HIGHTEST = 4;

    public static $priorities = [
        self::PRIORITY_LOW      => 'Низкий',
        self::PRIORITY_MIDDLE   => 'Средний',
        self::PRIORITY_HIGHT    => 'Высокий',
        self::PRIORITY_HIGHTEST => 'Наивысший'
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'files' => [
                'class'     => FileBehavior::className(),
                'attribute' => 'files',
            ],
            'tags'  => [
                'class' => TagBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }


    /**
     * Переопределяем Query
     *
     * @return TaskQuery
     */
    public static function find()
    {
        return new TaskQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unidoc_tasks_task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['strategy_id', 'project_id', 'name', 'status'], 'required'],
            [
                [
                    'strategy_id',
                    'project_id',
                    'parent_id',
                    'progress',
                    'priority',
                    'task_type',
                    'make_type',
                    'type',
                    'is_argue',
                    'status',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [['description', 'result', 'checklists', 'metric'], 'string'],
            [['price'], 'number'],
            [['started_at', 'finished_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['project_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Project::className(),
                'targetAttribute' => ['project_id' => 'id']
            ],
            [
                ['strategy_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Strategy::className(),
                'targetAttribute' => ['strategy_id' => 'id']
            ],
            [
                ['parent_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Task::className(),
                'targetAttribute' => ['parent_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
            ['formTags', 'safe'],
            [['monthDays', 'weekDays', 'customDates', 'time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('tasks', 'ID'),
            'strategy_id' => Yii::t('tasks', 'Стратегия'),
            'project_id'  => Yii::t('tasks', 'Проект'),
            'parent_id'   => Yii::t('tasks', 'Родитель'),
            'name'        => Yii::t('tasks', 'Наименование'),
            'description' => Yii::t('tasks', 'Описание'),
            'result'      => Yii::t('tasks', 'Результат'),
            'checklists'  => Yii::t('tasks', 'Checklists'),
            'progress'    => Yii::t('tasks', 'Прогресс'),
            'priority'    => Yii::t('tasks', 'Приоритет'),
            'task_type'   => Yii::t('tasks', 'Тип документа'),
            'make_type'   => Yii::t('tasks', 'Тип действия'),
            'type'        => Yii::t('tasks', 'Тип'),
            'price'       => Yii::t('tasks', 'Стоимость'),
            'time'        => Yii::t('tasks', 'Время'),
            'metric'      => Yii::t('tasks', 'Метрики цели'),
            'is_argue'    => Yii::t('tasks', 'Аргументировать результат'),
            'status'      => Yii::t('tasks', 'Статус'),
            'started_at'  => Yii::t('tasks', 'Дата начала'),
            'finished_at' => Yii::t('tasks', 'Дата окончания'),
            'created_at'  => Yii::t('tasks', 'Создан'),
            'updated_at'  => Yii::t('tasks', 'Изменен'),
            'created_by'  => Yii::t('tasks', 'Автор'),
            'updated_by'  => Yii::t('tasks', 'Изменил'),

            'monthDays'   => Yii::t('tasks', 'Дни месяца'),
            'weekDays'    => Yii::t('tasks', 'Дни недели'),
            'customDates' => Yii::t('tasks', 'Пользовательские даты'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStrategy()
    {
        return $this->hasOne(Strategy::className(), ['id' => 'strategy_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Task::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(TaskResult::className(), ['task_id' => 'id']);
    }

    /**
     * @return Query
     */
    public function getTagLink()
    {
        return $this->hasMany(TagLink::className(), ['object_id' => 'id'])
            ->andOnCondition(['object' => static::className()]);
    }

    /**
     * @return Query
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->via('tagLink');
    }

    /**
     * Получаем файлы привязанные к моделе
     *
     * @return Query
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['object_id' => 'id'])->where(['object' => static::className()]);
    }

    /**
     * Before save
     *
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->task_type = static::$taskType;
        return parent::beforeSave($insert);
    }

}
