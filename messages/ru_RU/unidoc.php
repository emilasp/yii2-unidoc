<?php
return [
    'Projects'        => 'Проекты',
    'Project'         => 'Проект',
    'Create Project'  => 'Создание проекта',
    'Strategies'      => 'Стратегии',
    'Strategy'        => 'Стратегия',
    'Create Strategy' => 'Создание стратегии',
    'Tasks'           => 'Задачи',
    'Task'            => 'Задача',
    'Create Task'     => 'Создание задачи',
];
