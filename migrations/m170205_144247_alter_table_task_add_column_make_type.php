<?php

use emilasp\unidoc\models\Task;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m170205_144247_alter_table_task_add_column_make_type extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->addColumn(Task::tableName(), 'make_type', $this->smallInteger(1)->unsigned()->comment('Тип действия'));
        $this->createIndex('idx_tasks_task__make_type', Task::tableName(), ['created_by', 'make_type']);

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropIndex('idx_tasks_task__make_type', Task::tableName());
        $this->dropColumn(Task::tableName(), 'make_type');

        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
