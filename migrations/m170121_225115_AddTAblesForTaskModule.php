<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m170121_225115_AddTAblesForTaskModule extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('unidoc_tasks_strategy', [
            'id'          => $this->primaryKey(11)->unsigned()->comment('ID'),
            'name'        => $this->string(255)->notNull()->comment('Наименование'),
            'description' => $this->text()->comment('Описание'),
            'status'      => $this->smallInteger(1)->unsigned()->notNull()->comment('Статус'),
            'created_at'  => $this->dateTime()->comment('Создан'),
            'updated_at'  => $this->dateTime()->comment('Изменен'),
            'created_by'  => $this->integer(11)->unsigned()->comment('Автор'),
            'updated_by'  => $this->integer(11)->unsigned()->comment('Изменил'),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_unidoc_tasks_strategy_created_by',
            'unidoc_tasks_strategy',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_unidoc_tasks_strategy_updated_by',
            'unidoc_tasks_strategy',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createTable('unidoc_tasks_project', [
            'id'          => $this->primaryKey(11)->unsigned()->comment('ID'),
            'strategy_id' => $this->integer(11)->unsigned()->notNull()->comment('Стратегия'),
            'name'        => $this->string(255)->notNull()->comment('Наименование'),
            'description' => $this->text()->comment('Описание'),
            'progress'    => $this->smallInteger(2)->unsigned()->comment('Прогресс'),
            'type'        => $this->smallInteger(1)->unsigned()->notNull()->comment('Type'),
            'status'      => $this->smallInteger(1)->unsigned()->notNull()->comment('Статус'),
            'created_at'  => $this->dateTime()->comment('Создан'),
            'updated_at'  => $this->dateTime()->comment('Изменен'),
            'created_by'  => $this->integer(11)->unsigned()->comment('Автор'),
            'updated_by'  => $this->integer(11)->unsigned()->comment('Изменил'),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_unidoc_tasks_project_strategy_id',
            'unidoc_tasks_project',
            'strategy_id',
            'unidoc_tasks_strategy',
            'id'
        );
        $this->addForeignKey(
            'fk_unidoc_tasks_project_created_by',
            'unidoc_tasks_project',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_unidoc_tasks_project_updated_by',
            'unidoc_tasks_project',
            'updated_by',
            'users_user',
            'id'
        );


        $this->createTable('unidoc_tasks_task', [
            'id'          => $this->primaryKey(11)->unsigned()->comment('ID'),
            'strategy_id' => $this->integer(11)->unsigned()->notNull()->comment('Стратегия'),
            'project_id'  => $this->integer(11)->unsigned()->comment('Проект'),
            'parent_id'   => $this->integer(11)->unsigned()->comment('Родитель'),
            'name'        => $this->string(255)->notNull()->comment('Наименование'),
            'description' => $this->text()->comment('Описание'),
            'result'      => $this->text()->comment('Результат'),
            'checklists'  => 'jsonb NULL DEFAULT \'[]\'',
            'progress'    => $this->smallInteger(2)->unsigned()->comment('Прогресс'),
            'priority'    => $this->smallInteger(1)->unsigned()->comment('Приоритет'),
            'type'        => $this->smallInteger(1)->unsigned()->comment('Тип документа'),
            'price'       => $this->decimal(10, 2)->unsigned()->comment('Стоимость'),
            'time'        => $this->integer(6)->unsigned()->comment('Время'),
            'status'      => $this->smallInteger(1)->unsigned()->notNull()->comment('Статус'),
            'started_at'  => $this->dateTime()->comment('Дата начала'),
            'finished_at' => $this->dateTime()->comment('Дата окончания'),
            'created_at'  => $this->dateTime()->comment('Создан'),
            'updated_at'  => $this->dateTime()->comment('Изменен'),
            'created_by'  => $this->integer(11)->unsigned()->comment('Автор'),
            'updated_by'  => $this->integer(11)->unsigned()->comment('Изменил'),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_unidoc_tasks_task_strategy_id',
            'unidoc_tasks_task',
            'strategy_id',
            'unidoc_tasks_strategy',
            'id'
        );
        $this->addForeignKey(
            'fk_unidoc_tasks_task_project_id',
            'unidoc_tasks_task',
            'project_id',
            'unidoc_tasks_project',
            'id'
        );
        $this->addForeignKey(
            'fk_unidoc_tasks_task_parent_id',
            'unidoc_tasks_task',
            'parent_id',
            'unidoc_tasks_task',
            'id'
        );

        $this->addForeignKey(
            'fk_unidoc_tasks_task_created_by',
            'unidoc_tasks_task',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_unidoc_tasks_task_updated_by',
            'unidoc_tasks_task',
            'updated_by',
            'users_user',
            'id'
        );

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('unidoc_tasks_task');
        $this->dropTable('unidoc_tasks_project');
        $this->dropTable('unidoc_tasks_strategy');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
