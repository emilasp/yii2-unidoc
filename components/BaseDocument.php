<?php

namespace frontend\modules\unidoc\components;

use Yii;
use yii\helpers\ArrayHelper;


class BaseDocument extends \frontend\modules\core\base\ActiveRecord
{
    const STATUS_SEARCH = 2;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public static $statuses = [
        self::STATUS_ACTIVE   => 'Виден',
        self::STATUS_SEARCH   => 'В поиске',
        self::STATUS_INACTIVE => 'Скрыт',
    ];

    public function behaviors()
    {
        return ArrayHelper::merge([], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'library_learn';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'comment'], 'string'],
            [['description', 'folder_id', 'type', 'status'], 'required'],
            [['folder_id', 'type', 'remember', 'status', 'created_by', 'updated_by'], 'integer'],
            [['updated_at', 'created_at', 'created_by', 'updated_by', 'learn'], 'safe'],
            [['name'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('library', 'ID'),
            'name'        => Yii::t('library', 'Наименование'),
            'description' => Yii::t('library', 'Знание'),
            'comment'     => Yii::t('library', 'Комментарий'),
            'folder_id'   => Yii::t('library', 'Папка'),
            'type'        => Yii::t('library', 'Тип'),
            'remember'    => Yii::t('library', 'Изучено'),
            'status'      => Yii::t('library', 'Статус'),
            'updated_at'  => Yii::t('library', 'Изменён'),
            'created_at'  => Yii::t('library', 'Создан'),
            'created_by'  => Yii::t('library', 'Кем создан'),
            'updated_by'  => Yii::t('library', 'Кем изменён'),
            'learn'       => Yii::t('library', 'Изучено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFolder()
    {
        return $this->hasOne(Folder::className(), ['id' => 'folder_id']);
    }

    /** Событие "перед сохранением"
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->learn) {
                if (is_numeric($this->remember)) {
                    $this->remember++;
                } else {
                    $this->remember = 1;
                }
            }
            return true;
        }
        return false;
    }
}
